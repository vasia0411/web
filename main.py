import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = 0#driver.find_element_by_id('th').text
    ah = 0#driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-27 09:31:01.850751
#2018-02-27 09:49:01.371202
#2018-02-27 10:04:01.586261
#2018-02-27 10:24:01.533025
#2018-02-27 10:51:01.680412
#2018-02-27 11:08:02.154941
#2018-02-27 11:32:01.927201
#2018-02-27 11:48:02.084979
#2018-02-27 12:09:01.706163
#2018-02-27 12:24:01.963957
#2018-02-27 12:38:01.836246
#2018-02-27 12:54:01.518790
#2018-02-27 13:15:01.724493
#2018-02-27 13:42:01.499770
#2018-02-27 14:03:01.287710
#2018-02-27 14:17:01.933082
#2018-02-27 14:34:02.147349
#2018-02-27 14:51:01.490112
#2018-02-27 15:10:01.606912
#2018-02-27 15:34:02.148293
#2018-02-27 15:56:01.373118
#2018-02-27 16:13:01.784614
#2018-02-27 16:35:01.677890
#2018-02-27 16:58:01.224732
#2018-02-27 17:49:02.054742
#2018-02-27 18:48:03.136314
#2018-02-27 19:33:02.125554
#2018-02-27 20:37:01.443594